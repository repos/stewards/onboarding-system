from . import config
from dataclasses import dataclass
import os


@dataclass(frozen=True)
class Role:
    description: str
    enabled_systems: list

    @staticmethod
    def new_from_dict(role: dict):
        return Role(
            role.get('description', ''),
            role.get('enabled_systems', {})
        )


class Roles:
    def __init__(self):
        self.roles = {}
    
    def load(self, specs):
        self.roles = {}
        for name, spec in specs.items():
            self.roles[name] = Role.new_from_dict(spec)
    
    def get_all(self):
        return self.roles.items()


roles = Roles()
roles.load(config.config_from_name('roles'))

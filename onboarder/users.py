from . import config
from dataclasses import dataclass
import os


@dataclass(frozen=True)
class User:
    sul_username: str
    ldap_uid: str
    email_address: str
    roles: tuple[str]

    @staticmethod
    def new_from_dict(user: dict):
        return User(
            user.get('sul_username'),
            user.get('ldap_uid'),
            user.get('email_address'),
            tuple(user.get('roles', []))
        )


class Users:
    def __init__(self):
        self.users = []
        self.users_by_role = {}
    
    def __add_user_to_role(self, user, role):
        if role not in self.users_by_role:
            self.users_by_role[role] = []
        self.users_by_role[role].append(user)
    
    def load(self, user_specs):
        self.users = []
        self.users_by_role = {}

        for user_spec in user_specs:
            user = User.new_from_dict(user_spec)
            self.users.append(user)
            for role in user.roles:
                self.__add_user_to_role(user, role)
    
    def get_users_with_role(self, role) -> list[User]:
        return self.users_by_role.get(role, [])

users = Users()
users.load(config.config_from_name('users'))

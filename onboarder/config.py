import os
import yaml


def config_from_path(filepath, require_config = False):
    if not os.path.isfile(filepath):
        if require_config:
            raise Exception(f'Config {filepath} is required, but not found')
        return {}
    with open(filepath, 'r') as f:
        return yaml.safe_load(f)


def config_from_name(name):
    config_paths = app.get('config_paths')
    return config_from_path(config_paths.get(
        name,
        os.path.join(
            os.path.dirname(__file__),
            '..',
            'config',
            f'{name}.yaml'
        )
    ))


app = config_from_path(os.environ.get(
    'STEWARD_ONBOARDER_CONFIG',
    '/etc/steward-onboarder/steward-onboarder.yaml'
), True)

from ..config import app

import requests

class Api:
    def __init__(self, api_url):
        self.api_url = api_url
    
    def request(self, method='GET', data=None, *args, **kwargs):
        data['formatversion'] = 2
        data['format'] = 'json'

        if method == 'GET':
            kwargs['params'] = data
        else:
            kwargs['data'] = data

        r = requests.request(
            method,
            url=self.api_url,
            *args,
            **kwargs
        )
        r.raise_for_status()
        return r


class GlobalGroupClient:
    def __init__(self):
        self.api = Api('https://meta.wikimedia.org/w/api.php')
    
    def get_members(self, group_name, continue_data={}):
        payload = {
            'action': 'query',
            'list': 'globalallusers',
            'agugroup': group_name,
            'agulimit': 10,
        }
        payload.update(continue_data)

        r = self.api.request(data=payload)
        data = r.json()
        result = data.get('query', {}).get('globalallusers', [])
        
        if 'continue' in data:
            result += self.get_members(group_name, data['continue'])
        
        return result


class LocalGroupClient:
    def __init__(self, api_url):
        self.api = Api(api_url)

    def get_members(self, group_name, continue_data={}):
        payload = {
            'action': 'query',
            'list': 'allusers',
            'augroup': group_name,
            'aulimit': 10,
        }
        payload.update(continue_data)

        r = self.api.request(data=payload)
        data = r.json()
        result = data.get('query', {}).get('allusers', [])

        if 'continue' in data:
            result += self.get_members(group_name, data['continue'])

        return result

from . import config, users

class OnboarderContext:
    def __init__(self, users_db_path):
        self.users = users.Users(users_db_path)
        self.users.load()
    

from .. import config
from ..roles import roles
from ..users import users

from abc import ABC, abstractmethod
from enum import Enum
import os

class SystemFactory:
    def __init__(self):
        self.__creators = {}
    
    def register(self, name, creator):
        self.__creators[name] = creator
    
    def get_keys(self) -> list[str]:
        return list(self.__creators.keys())
    
    def get(self, name):
        creator = self.__creators.get(name)
        if not creator:
            raise ValueError(name)
        return creator(name)

system_factory = SystemFactory()


def register_system(name):
    def wrapper(clsName):
        system_factory.register(name, clsName)
    return wrapper


class System(ABC):

    def __init__(self, name):
        self.name = name
        self.export_path = os.path.join(config.app.get('export_base_dir', '/tmp'), self.name)
    
    def get_members(self):
        result = {}
        for rolename, role in roles.get_all():
            if self.name in role.enabled_systems:
                members = set(users.get_users_with_role(rolename))
                for system in role.enabled_systems[self.name]:
                    if system not in result:
                        result[system] = set()
                    result[system] = result[system].union(members)
        return result

    @abstractmethod
    def update_system(self):
        pass


# load individual systems, so that they can get registered
from . import gitlab
from . import ldap
from . import phabricator
from . import mailman

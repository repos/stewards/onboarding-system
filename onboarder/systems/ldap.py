from . import System, register_system

import shutil
import os

@register_system('ldap_group')
class LdapGroup(System):
    def __init__(self, name):
        super().__init__(name)
    
    def update_system(self):
        if os.path.exists(self.export_path):
            shutil.rmtree(self.export_path)
        os.mkdir(self.export_path)

        result = self.get_members()
        for group, members in result.items():
            group_path = os.path.join(self.export_path, f'{group}.txt')
            with open(group_path, 'w') as f:
                uids = list({m.ldap_uid for m in members if m.ldap_uid is not None})
                uids.sort()
                f.write('\n'.join(uids))
                f.write('\n')

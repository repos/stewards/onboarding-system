from . import System, register_system

import shutil
import os

@register_system('mailman_list')
class MailmanList(System):
    def __init__(self, name):
        super().__init__(name)

    def update_system(self):
        if os.path.exists(self.export_path):
            shutil.rmtree(self.export_path)
        os.mkdir(self.export_path)

        result = self.get_members()
        for mailman_list, members in result.items():
            list_name, domain_name = mailman_list.split('@', 2)
            domain_path = os.path.join(self.export_path, domain_name)
            if not os.path.exists(domain_path):
                os.mkdir(domain_path)

            with open(os.path.join(domain_path, list_name), 'w') as f:
                emails = list({f'{x.sul_username} <{x.email_address.lower()}>' for x in members if x.email_address is not None})
                emails.sort()
                f.write('\n'.join(emails))
                f.write('\n')

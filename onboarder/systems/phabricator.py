from . import System, register_system
from .. import config

import logging
import phabricator

class BasePhabricator(System):
    def __init__(self, name):
        super().__init__(name)
        system_config = config.app.get('phabricator_api', {})
        self.api = phabricator.Phabricator(host=system_config.get('api_url'), token=system_config.get('api_token'))
        self.perform_additions = True
        self.perform_removals = True

    def _phid_to_users(self, phids, chunk_size=50):
        chunks = (phids[x:x + chunk_size] for x in range(0, len(phids), chunk_size))
        for chunk in chunks:
            yield from self.api.user.search(constraints={"phids": chunk}, limit=chunk_size + 10)['data']

    def get_project_data(self, name):
        project_data = self.api.project.search(constraints={"name": name}, attachments={"members": True})['data']
        if len(project_data) != 1:
            logging.error(f'Project {name} not found in Phabricator')
            return None

        return project_data[0]

    def get_project_members(self, project_data):
        member_phids = [x['phid'] for x in project_data['attachments']['members']['members']]
        return list(self._phid_to_users(member_phids))

    def get_users_from_mediawiki_usernames(self, usernames, chunk_size=50):
        usernames = list(usernames)
        chunks = (usernames[x:x + chunk_size] for x in range(0, len(usernames), chunk_size))
        for chunk in chunks:
            yield from self.api.user.mediawikiquery(names=chunk, limit=chunk_size + 20)

    def update_system(self):
        members = self.get_members()
        for project, users in members.items():
            project_data = self.get_project_data(project)
            current_members = {
                x['fields']['mediawikiUsername']: x
                for x in self.get_project_members(project_data)
            }
            new_members = {
                x['mediawiki_username']: x
                for x in self.get_users_from_mediawiki_usernames([x.sul_username for x in users])
            }

            transactions = []
            to_add = []
            for mw_username, phab_user in new_members.items():
                if phab_user['mediawiki_username'] not in current_members:
                    if not self.perform_additions:
                        logging.info(f'Would add {mw_username} to {project}, but action is disabled')
                        continue
                    logging.info(f'Adding {mw_username} to {project}')
                    to_add.append(phab_user['phid'])

            if to_add != []:
                transactions.append({"type": "members.add", "value": to_add})

            to_remove = []
            for mw_username, phab_user in current_members.items():
                if mw_username not in new_members:
                    if not self.perform_removals:
                        logging.info(f'Would remove {mw_username} from {project}, but action is disabled')
                        continue
                    logging.info(f'Removing {mw_username} from {project}')
                    to_remove.append(phab_user['phid'])

            if to_remove != []:
                transactions.append({"type": "members.remove", "value": to_remove})

            if transactions != []:
                self.api.project.edit(
                    transactions=transactions,
                    objectIdentifier=project_data['phid']
                )

@register_system('phabricator')
class FullPhabricator(BasePhabricator):
    pass

@register_system('phabricator_remove_only')
class RemoveOnlyPhabricator(BasePhabricator):
    def __init__(self, name):
        super().__init__(name)
        self.perform_additions = False
        self.perform_removals = True

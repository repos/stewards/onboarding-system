from . import System, register_system
from .. import config

import logging
import requests
import urllib.parse

class GitlabApi:
    ACCESS_LEVEL_DEVELOPER = 30

    def __init__(self, api_root, token):
        self.api_root = api_root
        self.token = token

    def _get(self, url, *args, **kwargs):
        kwargs = self._add_headers(kwargs)
        return requests.get(self._full_url(url), *args, **kwargs)

    def _post(self, url, *args, **kwargs):
        kwargs = self._add_headers(kwargs)
        return requests.post(self._full_url(url), *args, **kwargs)

    def _put(self, url, *args, **kwargs):
        kwargs = self._add_headers(kwargs)
        return requests.put(self._full_url(url), *args, **kwargs)

    def _delete(self, url, *args, **kwargs):
        kwargs = self._add_headers(kwargs)
        return requests.delete(self._full_url(url), *args, **kwargs)

    def _full_url(self, url):
        return '{}{}'.format(self.api_root, url)

    def _quoted_path(self, path):
        return urllib.parse.quote(path, safe='')

    def _add_headers(self, kwargs):
        if 'headers' not in kwargs:
            kwargs['headers'] = {}
        kwargs['headers']['Private-Token'] = self.token
        return kwargs

    def get_user_id(self, username):
        response = self._get('/users?username={}'.format(username))
        response.raise_for_status()
        users = response.json()
        if users == []:
            return None
        user = users[0]
        return user['id']

    def get_group_members(self, group):
        r = self._get(f'/groups/{self._quoted_path(group)}/members')
        r.raise_for_status()
        return r.json()

    def add_user_to_group(self, group, user_id, access_level):
        r = self._post(f'/groups/{self._quoted_path(group)}/members', data={
            'user_id': user_id,
            'access_level': access_level,
        })
        r.raise_for_status()
        return r

    def remove_user_from_group(self, group, user_id):
        r = self._delete(f'/groups/{self._quoted_path(group)}/members/{user_id}')
        r.raise_for_status()
        return r


@register_system('gitlab_group')
class GitlabGroup(System):
    MANAGED_ACCESS_LEVEL = GitlabApi.ACCESS_LEVEL_DEVELOPER

    def __init__(self, name):
        super().__init__(name)
        system_config = config.app.get('gitlab_api', {})
        self.gitlab_api = GitlabApi(system_config.get('api_root'), system_config.get('api_token'))
    
    def update_system(self):
        members = self.get_members()
        for gitlab_group, users in members.items():
            current_members = {
                x['username']: x
                for x in self.gitlab_api.get_group_members(gitlab_group)
            }
            new_members = {x.ldap_uid for x in users if x.ldap_uid is not None}

            for uid in new_members:
                if uid not in current_members:
                    user_id = self.gitlab_api.get_user_id(uid)
                    if user_id is None:
                        # TODO: Provision the GitLab account automatically?
                        logging.error(f'Failed to add {uid} to {gitlab_group}, GitLab account does not exist.')
                        continue

                    logging.info(f'Adding {uid} to {gitlab_group}')
                    self.gitlab_api.add_user_to_group(gitlab_group, user_id, self.MANAGED_ACCESS_LEVEL)

            for username, user in current_members.items():
                if user['access_level'] != self.MANAGED_ACCESS_LEVEL:
                    logging.info(f'Skipping {username}, their access level is not managed.')
                    continue
                
                if username not in new_members:
                    logging.info(f'Removing {username} from {gitlab_group}, no longer authorised')
                    self.gitlab_api.remove_user_from_group(gitlab_group, user['id'])

#!/usr/bin/env python3

import click
import os
import logging
from onboarder import OnboarderContext
from onboarder.api.mediawiki import GlobalGroupClient, LocalGroupClient
from onboarder.systems import system_factory
from onboarder.users import users

logging.basicConfig(level=logging.INFO)

@click.group()
def onboarder():
    pass


@onboarder.command(help='Verify that exactly the right users have a given role')
@click.option('--role', required=True, help='Role name to audit')
@click.option('--global-group', help='Global group name to use')
@click.option('--metawiki-group', help='Metawiki group name to use')
def audit_role_membership(role, global_group, metawiki_group):
    actual_usernames = set([x.sul_username for x in users.get_users_with_role(role)])

    expected_usernames = set()
    if global_group:
        client = GlobalGroupClient()
        expected_usernames = expected_usernames.union(
            set([x.get('name') for x in client.get_members(global_group)])
        )

    if metawiki_group:
        client = LocalGroupClient('https://meta.wikimedia.org/w/api.php')
        expected_usernames = expected_usernames.union(
            set([x.get('name') for x in client.get_members(metawiki_group)])
        )

    missing_names = expected_usernames - actual_usernames
    extra_names = actual_usernames - expected_usernames

    if missing_names:
        click.echo('ERROR: Detected missing users:')
        for name in missing_names:
            click.echo(f'\t* {name}')

    if extra_names:
        click.echo('ERROR: Detected extra users')
        for name in extra_names:
            click.echo(f'\t* {name}')


@onboarder.command()
@click.option('--system', '-s', 'systems', help='System to update (defaults to update all)', multiple=True)
def update(systems):
    if len(systems) == 0:
        systems = system_factory.get_keys()

    for system_key in systems:
        click.echo(f'== Updating {system_key}')
        system = system_factory.get(system_key)
        system.update_system()

if __name__ == '__main__':
    onboarder()
